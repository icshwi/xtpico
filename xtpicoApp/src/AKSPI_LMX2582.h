/*
 * AKSPI_LMX2582.h
 *
 *  Created on: Jul 11, 2019
 *      Author: hinkokocevar
 */

#ifndef _AKSPI_LMX2582_H_
#define _AKSPI_LMX2582_H_


#include "AKSPI.h"

#define AKSPI_LMX2582_WriteAllString					"AKSPI_LMX2582_WRITE_ALL"
#define AKSPI_LMX2582_ReadAllString						"AKSPI_LMX2582_READ_ALL"
#define AKSPI_LMX2582_MUXOutSelString					"AKSPI_LMX2582_MUXOUT_SEL"
#define AKSPI_LMX2582_RegConfigString					"AKSPI_LMX2582_REG_CONFIG"

/*
 * Chip			: TI LMX2582
 * Function		: PLL/VCO
 * Bus			: SPI
 * Access		: TCP/IP socket on AK-NORD XT-PICO-SX
 */
class AKSPI_LMX2582: public AKSPI {
public:
	AKSPI_LMX2582(const char *portName, const char *ipPort,
			int priority, int stackSize);
	virtual ~AKSPI_LMX2582();

	/* These are the methods that we override from AKSPI */
	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
	void report(FILE *fp, int details);
	/* These are new methods */

protected:
	/* Our parameter list */
	int AKSPI_LMX2582_WriteAll;
#define FIRST_AKSPI_LMX2582_PARAM AKSPI_LMX2582_WriteAll
	int AKSPI_LMX2582_ReadAll;
	int AKSPI_LMX2582_MUXOutSel;
	int AKSPI_LMX2582_RegConfig;

private:
	asynStatus readAll(int addr);
	asynStatus writeAll(int addr);
};

#endif /* _AKSPI_LMX2582_H_ */
