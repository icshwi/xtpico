/*
 * AKI2C_AD5144A.h
 *
 *  Created on: February 7, 2022
 *      Author: krisztianloki
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <alarm.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynPortDriver.h>
#include <AKI2C_AD5144A.h>

static const char *driverName = "AKI2C_AD5144A";
static const double tapPoints = 256.0;
static const double res10k  = 10000.0;
static const double res100k = 100000.0;


static void exitHandler(void *drvPvt) {
	AKI2C_AD5144A *pPvt = (AKI2C_AD5144A *)drvPvt;
	pPvt->lock();
	delete pPvt;
}

RDAC::RDAC(asynPortDriver *driver, int address, const char *readString, const char *valueString) {
	this->address = address;
	driver->createParam(readString,		asynParamInt32,		&readParam);
	driver->createParam(valueString,	asynParamFloat64,	&valueParam);
	index = (address >> 8) + 1;
}

const RDAC* AKI2C_AD5144A::readParamToRdac(int param) {
	for (unsigned int i = 0; i < sizeof(rdacs) / sizeof(rdacs[0]); ++i) {
		if (rdacs[i]->readParam == param)
			return rdacs[i];
	}

	return NULL;
}

const RDAC* AKI2C_AD5144A::valueParamToRdac(int param) {
	for (unsigned int i = 0; i < sizeof(rdacs) / sizeof(rdacs[0]); ++i) {
		if (rdacs[i]->valueParam == param)
			return rdacs[i];
	}

	return NULL;
}

asynStatus AKI2C_AD5144A::write(int addr, unsigned short cmd, unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char data[2] = {0};
	int devAddr;

	getIntegerParam(addr, AKI2CDevAddr, &devAddr);

	data[0] = (cmd >> 8) & 0xFF;
	data[1] = cmd & 0xFF;
	D(printf("WRITE command 0x%04X\n", cmd));
	status = xfer(addr, AK_REQ_TYPE_WRITE, devAddr, 0, data, &len, 0);

	return status;
}

asynStatus AKI2C_AD5144A::writeValue(int addr, const RDAC *rdac, double val) {
	asynStatus status = asynSuccess;
	unsigned short cmd;
	unsigned short data = 0;
	static const char *functionName = "writeValue";

	data = (unsigned short)((val * tapPoints) / mMaxRes);
	if (data > 255) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s::%s data (%d) > 255 (calculated from %f)\n", driverName, functionName, data, val);
		return asynError;
	}

	cmd = AKI2C_AD5144A_WR_RDAC_CMD | rdac->address | data;
	D(printf("resistance %f, maxRes %f, taps %f, val %f, D %d: cmd 0x%04X\n",
			val, mMaxRes, tapPoints, val, data, cmd));

	status = write(addr, cmd, 2);
	if (status) {
		return status;
	}

	D(printf("wrote RDAC %d 0x%04X (%d)\n", rdac->index, data, data));

	D(printf("reading back RDAC %d...\n", rdac->index));
	status = readValue(addr, rdac);

	return status;
}

asynStatus AKI2C_AD5144A::read(int addr, unsigned short cmd, unsigned short *val, unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char data[2] = {0};
	int devAddr;

	getIntegerParam(addr, AKI2CDevAddr, &devAddr);

	/* Read register - 1 byte */
	status = xfer(addr, AK_REQ_TYPE_READ, devAddr, 2, data, &len, cmd);
	if (status) {
		return status;
	}

	*val = (mResp[2] & 0xFF);

	D(printf("cmd 0x%04X, READ value 0x%04X\n", cmd, *val));

	return status;
}

asynStatus AKI2C_AD5144A::readValue(int addr, const RDAC *rdac) {
	asynStatus status = asynSuccess;
	unsigned short data;
	unsigned short cmd = AKI2C_AD5144A_RD_CMD | rdac->address | AKI2C_AD5144A_RD_RDAC;
	double res;

	status = read(addr, cmd, &data, 1);
	if (status) {
		setParamAlarmStatus(addr, rdac->valueParam, COMM_ALARM);
		setParamAlarmSeverity(addr, rdac->valueParam, INVALID_ALARM);
		return status;
	}

	res = ((double)data / tapPoints) * mMaxRes;

	D(printf("resistance %f, maxRes %f, taps %f, D %d: cmd 0x%04X\n",
			res, mMaxRes, tapPoints, data, cmd));

	setParamAlarmStatus(addr, rdac->valueParam, NO_ALARM);
	setParamAlarmSeverity(addr, rdac->valueParam, NO_ALARM);
	setDoubleParam(addr, rdac->valueParam, res);

	return status;
}

asynStatus AKI2C_AD5144A::writeInt32(asynUser *pasynUser, epicsInt32 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeInt32";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	D(printf("function %d, addr %d, value %d\n", function, addr, value));
	status = setIntegerParam(addr, function, value);

	const RDAC *rdac;
	if ((rdac = readParamToRdac(function))) {
		status = readValue(addr, rdac);
	} else if (function == AKI2C_AD5144A_MaxRes) {
		switch (value) {
			case AKI2C_AD5144A_MAXRES_10k:
				mMaxRes = res10k;
				break;
			case AKI2C_AD5144A_MAXRES_100k:
				mMaxRes = res100k;
				break;
			default:
				status = asynError;
		}
	} else if (function < FIRST_AKI2C_AD5144A_PARAM) {
		/* If this parameter belongs to a base class call its method */
		status = AKI2C::writeInt32(pasynUser, value);
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%s function=%d, addr=%d, value=%d\n",
			driverName, functionName, pasynManager->strStatus(status), function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%d\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}

asynStatus AKI2C_AD5144A::writeFloat64(asynUser *pasynUser, epicsFloat64 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeFloat64";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	D(printf("function %d, addr %d, value %f\n", function, addr, value));
	status = setDoubleParam(addr, function, value);

	if (function < FIRST_AKI2C_AD5144A_PARAM) {
		/* If this parameter belongs to a base class call its method */
		status = AKI2C::writeFloat64(pasynUser, value);
	} else {
		const RDAC *rdac = valueParamToRdac(function);
		if (rdac == NULL) {
			status = asynError;
		} else {
			status = writeValue(addr, rdac, value);
		}
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%s function=%d, addr=%d, value=%f\n",
			driverName, functionName, pasynManager->strStatus(status), function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%f\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}

void AKI2C_AD5144A::report(FILE *fp, int details) {

	fprintf(fp, "AKI2C_AD5144A %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKI2C::report(fp, details);
}

/** Constructor for the AKI2C_AD5144A class.
  * Calls constructor for the AKI2C base class.
  * All the arguments are simply passed to the AKI2C base class.
  */
AKI2C_AD5144A::AKI2C_AD5144A(const char *portName, const char *ipPort,
		int devCount, const char *devInfos, int priority, int stackSize)
	: AKI2C(portName,
		ipPort,
		devCount, devInfos,
		0, /* no new interface masks beyond those in AKBase */
		0, /* no new interrupt masks beyond those in AKBase */
		ASYN_CANBLOCK | ASYN_MULTIDEVICE,
		1, /* autoConnect YES */
		priority, stackSize),
	rdacs()
{
	asynStatus status = asynSuccess;

	rdacs[0] = new RDAC(this, AKI2C_AD5144A_ADDR_RDAC1, AKI2C_AD5144A_RDAC1_ReadString, AKI2C_AD5144A_RDAC1_ValueString);
	rdacs[1] = new RDAC(this, AKI2C_AD5144A_ADDR_RDAC2, AKI2C_AD5144A_RDAC2_ReadString, AKI2C_AD5144A_RDAC2_ValueString);
	rdacs[2] = new RDAC(this, AKI2C_AD5144A_ADDR_RDAC3, AKI2C_AD5144A_RDAC3_ReadString, AKI2C_AD5144A_RDAC3_ValueString);
	rdacs[3] = new RDAC(this, AKI2C_AD5144A_ADDR_RDAC4, AKI2C_AD5144A_RDAC4_ReadString, AKI2C_AD5144A_RDAC4_ValueString);

	mMaxRes = res10k;

	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	FIRST_AKI2C_AD5144A_PARAM = rdacs[0]->readParam;
	createParam(AKI2C_AD5144A_MaxResString,		asynParamInt32,		&AKI2C_AD5144A_MaxRes);

	for (int i = 0; i < devCount; i++) {
		setIntegerParam(i, AKI2C_AD5144A_MaxRes, AKI2C_AD5144A_MAXRES_10k);
		/* Do callbacks so higher layers see any changes */
		callParamCallbacks(i, i);
	}

	if (status) {
		disconnect(pasynUserSelf);
		I(printf("init FAIL!\n"));
		return;
	}

	I(printf("init OK!\n"));
}

AKI2C_AD5144A::~AKI2C_AD5144A() {
	I(printf("shut down ..\n"));
	delete rdacs[0];
	delete rdacs[1];
	delete rdacs[2];
	delete rdacs[3];
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int AKI2CAD5144AConfigure(const char *portName, const char *ipPort,
		int devCount, const char *devInfos, int priority, int stackSize) {
	new AKI2C_AD5144A(portName, ipPort, devCount, devInfos, priority, stackSize);
	return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",		iocshArgString};
static const iocshArg initArg1 = { "ipPort",		iocshArgString};
static const iocshArg initArg2 = { "devCount",		iocshArgInt};
static const iocshArg initArg3 = { "devInfos",		iocshArgString};
static const iocshArg initArg4 = { "priority",		iocshArgInt};
static const iocshArg initArg5 = { "stackSize",		iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
											&initArg1,
											&initArg2,
											&initArg3,
											&initArg4,
											&initArg5};
static const iocshFuncDef initFuncDef = {"AKI2CAD5144AConfigure", 6, initArgs};
static void initCallFunc(const iocshArgBuf *args) {
	AKI2CAD5144AConfigure(args[0].sval, args[1].sval,
			args[2].ival, args[3].sval, args[4].ival, args[5].ival);
}

void AKI2CAD5144ARegister(void) {
	iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(AKI2CAD5144ARegister);

} /* extern "C" */
