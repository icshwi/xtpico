#!/usr/bin/python3

import requests
import sys
import argparse
# from lxml.html import etree
# from io import StringIO, BytesIO
import re
# import graphviz
from collections import OrderedDict
from collections import namedtuple


class Action:
    def __init__(self, act, txt):
        self.act = act
        self.txt = txt
        self.key = None
        self.value = None
        m = re.search('(^[a-zA-Z0-9]) = (.*)', self.txt)
        if m:
            self.key = m.group(1)
            self.value = m.group(2)
        else:
            self.key = self.txt
            self.value = ''

    def __str__(self):
        return '%s: %s' % (self.key, self.value)


class Line:
    def __init__(self, txt):
        self.txt = txt
        self.show = True
        self.act = None
        # command: [single ASCII char] [space] [assign sign] [space] [command text]
        m = re.search('(^[a-zA-Z0-9]) = (.*)', self.txt)
        # if m:
        #     self.act = Action(m.group(1), m.group(2))

    def __str__(self):
        if self.act:
            return self.act.__str__()
        else:
            return self.txt


class Page1:
    def __init__(self, txt):
        self.title = None
        self.txt = txt
        self.lines = []
        self.actions = OrderedDict()
        self.parse()

    def __str__(self):
        s = 'Title: %s\n' % self.title
        for l in self.lines:
            s += '>> %s\n' % l
        for k, v in self.actions.items():
            s += ' %s: %s\n' % (k, v)
        return s

    def parse(self):
        # the start of the monitor data is line with at least 5x '='
        # the end of the monitor data is line with at least 5x '-'
        s = self.txt.find('=====')
        e = self.txt.find('[Q = QUIT]', s)
        # print(r.text[s:e])

        tmp = self.txt[s:e]
        print(tmp)
        tmp = tmp.split('\r\n')
        print(tmp)
        raw_lines = []
        for raw_line in tmp:
            # line = re.sub(r'\s+', ' ', raw_line).strip()
            raw_line = raw_line.strip()
            if len(raw_line):
                raw_lines.append(raw_line)
                print('LINE:', raw_line)
        print(raw_lines)

        # first line is title ( ========= TITLE ========)
        self.title = raw_lines[0].strip('=').strip()
        print('page title:', self.title)
        raw_lines.pop(0)
        for raw_line in raw_lines:
            if raw_line.startswith('-----') or raw_line.startswith('For example:'):
                continue
            line = Line(raw_line)
            self.lines.append(line)
            if line.act:
                act = line.act
                self.actions[act.key] = act


class XTPicoHTTP:
    def __init__(self, host):
        self.host = 'http://%s' % host
        self.auth = requests.auth.HTTPBasicAuth('xt', 'xt')

    def page(self, resp):
        """ parse Response object text for useful info """
        if resp.status_code != 200:
            return None

        pg = Page(resp.text)
        return pg

    def get(self, url):
        r = requests.get(self.host + '/' + url,
                         auth=self.auth)
        # print(r)
        return self.page(r)

    def put(self, url, inputLine):
        data = {'inputLine': inputLine, 'B1': 'Send'}
        # r = requests.post('http://192.168.0.137/SendLine', auth=requests.auth.HTTPBasicAuth('xt', 'xt'), data=data)
        r = requests.post(self.host + '/' + url,
                          auth=self.auth,
                          data=data)
        print(r)
        return self.page(r)


class XTPicoWeb(XTPicoHTTP):
    def __init__(self, host):
        super().__init__(host)

    def monitor(self):
        try:
            r = self.get('monitor.htm')
        except requests.exceptions.ConnectionError as e:
            print(e)
            return None

        print(r)

    def navigate(self, inputLine):
        try:
            r = self.put('SendLine', inputLine)
        except requests.exceptions.ConnectionError as e:
            print(e)
            return None

        print(r)
















# Point = namedtuple('Point', ['x', 'y'])
Route = namedtuple('Route', ['name', 'path', 'get', 'set'])
Page = namedtuple('Page', ['title', 'lines'])


def get_routes():
    return {
        # always add %s for setter
        'RESTART':      Route('RESTART', (), 'R = Restart Interface', 'R%s'),
        'INTF1.BUS':    Route('INTF1.BUS', ('I', '1', '1'), ' = BUS', 'c=%s')
    }


class Handler:
    def __init__(self, host):
        self.host = 'http://%s' % host
        self.auth = requests.auth.HTTPBasicAuth('xt', 'xt')

    def page(self, resp):
        """ parse Response object text for useful info """
        if resp.status_code != 200:
            return False

        # the start of the monitor data is line with at least 5x '='
        # the end of the monitor data is line with at least 5x '-'
        s = resp.text.find('=====')
        e = resp.text.find('[Q = QUIT]', s)
        txt = resp.text[s:e]
        print(txt)
        lines = [l.strip() for l in txt.split('\r\n') if len(l)]
        # get the page title
        m = re.search('[=]* ([a-zA-Z0-9\s]*) [=]*$', lines[0])
        # import pdb; pdb.set_trace()
        if m:
            title = m.group(1)
            return Page(title, lines)
        return Page(None, None)

    def get(self, url):
        r = requests.get(self.host + '/' + url,
                         auth=self.auth)
        # print(r)
        return self.page(r)

    def put(self, url, inputLine):
        data = {'inputLine': inputLine, 'B1': 'Send'}
        r = requests.post(self.host + '/' + url,
                          auth=self.auth,
                          data=data)
        # print(r)
        return self.page(r)


class Web():
    def __init__(self, host, route, action='get', value=None):
        self.ret = -1
        self.h = Handler(host)
        self.routes = get_routes()
        self.action = action
        self.value = value
        if route in self.routes:
            self.route = self.routes[route]
        print(self.route)
        self.ret = self.execute()

    def execute(self):
        if not self.route:
            return False
        page = self.monitor()
        print('we are on %s page' % page.title)
        # do we need to go home?
        while page.title != 'MAIN MENU':
            # go back
            page = self.navigate('Q')
            if page.title == 'MAIN MENU':
                break

        page = self.monitor()
        assert(page.title == 'MAIN MENU')
        print('we are on %s page' % page.title)

        # get to the page
        for p in self.route.path:
            page = self.navigate(p)

        # if we need to set the prameter do it now
        if self.action == 'set':
            page = self.navigate(self.route.set % self.value)
        # always read back the value
        for l in page.lines:
            if l.find(self.route.get) != -1:
                print('got result:', l)
                return l
        return ""

    def monitor(self):
        try:
            page = self.h.get('monitor.htm')
        except requests.exceptions.ConnectionError as e:
            print(e)
            return None
        finally:
            print(page)
            return page

    def navigate(self, inputLine):
        try:
            print('SendLine:', inputLine)
            page = self.h.put('SendLine', inputLine)
        except requests.exceptions.ConnectionError as e:
            print(e)
            return None
        finally:
            print(page)
            return page


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('host', help='IP address or DNS name', default=None)
    parser.add_argument('action', help='get/set action', default=None)
    parser.add_argument('param', help='parameter name', default=None)
    parser.add_argument('value', help='parameter value (for set only)', nargs='?', default=None)
    args = parser.parse_args()
    print(args)
    if args.action == 'set' and not args.value:
        print('no value specified for set action')
        sys.exit(1)

    w = Web(args.host, args.param, args.action, args.value)
    r = 0 if len(w.ret) else -1
    sys.exit(r)
