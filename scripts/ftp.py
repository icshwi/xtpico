from ftplib import FTP, error_perm
import sys
import argparse


class XTPicoFTP:
    def __init__(self, host):
        self.ftp = FTP(host)
        self.ftp.login('xt', 'xt')
        print(self.ftp.getwelcome())
        # change directory to VOLUME1
        self.ftp.cwd('VOLUME1')

    def list(self):
        # LIST files
        items = []
        self.ftp.retrlines('LIST', items.append)
        print('listing %d file(s) ..' % (len(items)))
        for i in items:
            print(i)

    def download(self, remfile, locfile):
        print('downloading %s -> %s ..' % (remfile, locfile))
        self.ftp.retrbinary('RETR %s' % remfile,
                            open(locfile, 'wb').write)

    def upload(self, locfile, remfile):
        print('uploading %s -> %s ..' % (locfile, remfile))
        self.ftp.storbinary('STOR %s' % remfile,
                            open(locfile, 'rb'))

    def remove(self, remfile):
        print('removing %s ..' % (remfile))
        self.ftp.delete('%s' % remfile)


class XTPicoConfig(XTPicoFTP):
    def __init__(self, host):
        super().__init__(host)
        self.conffile = 'CONFIG.EEP'

    def get(self, localfile):
        self.download(self.conffile, localfile)

    def put(self, localfile):
        self.upload(localfile, self.conffile)

    def delete(self):
        self.remove(self.conffile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-f', '--ftp-host', help='IP address or DNS name')
    parser.add_argument('-n', '--filename', help='local/remote filename')
    parser.add_argument('command', help='action to perform', nargs='?', default=None)
    args = parser.parse_args()
    print(args)

    if not args.ftp_host:
        print('no FTP host specified')
        sys.exit(1)

    ftp = XTPicoConfig(args.ftp_host)

    if args.command == 'list':
        ftp.list()
    elif args.command == 'get':
        if not args.filename:
            print('no local file specified')
            sys.exit(1)
        try:
            ftp.get(args.filename)
        except error_perm as e:
            print(e)
            sys.exit(1)
    elif args.command == 'put':
        if not args.filename:
            print('no local file specified')
            sys.exit(1)
        try:
            ftp.put(args.filename)
        except error_perm as e:
            print(e)
            sys.exit(1)
        except FileNotFoundError as e:
            print(e)
            sys.exit(1)
    elif args.command == 'del':
        try:
            ftp.delete()
        except error_perm as e:
            print(e)
            sys.exit(1)
    else:
        print('missing command!')

    sys.exit(0)
